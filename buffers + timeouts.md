# Buffer

* Buffer là vùng lưu trữ dữ liệu tạm thời trong khi chờ để được chuyển đến một vị trí khác.
* Buffer thường nằm bên trong ổ RAM máy tính. Buffer được phát triển để ngăn chặn sự tắc nghẽn dữ liệu khi gửi đi từ một port.
    - e.g. 
        - Server Request:
        nginx receive request (which its read from tcp:80) -> write request data to memory (buffering) 
            - if buffer to small -> it write some of it to disk (bufffer overflow to HD)
        - Server Respond:
        nginx response to request (which its read from disk) -> write response to memory (buffering the file) -> send data to client from memory
    - This happens as the name implies to create a buffer or layer of protection between reading and writing of data

# Timeouts 
* Cut off time for a given event
    - e.g. If receiving a request from a client stop after a certain number of seconds thus preventing a client from sending an endless stream of data and eventually breaking the server.