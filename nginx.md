# NGINX

## Webserver, Reserve Proxy Server, LoadBalancing, API Gateway

## NGINX vs APACHE

* Asynchronously, event‑driven approach to handling connections

    -> Server multiple request concurrently

    -> Can't embed server side programming into it process

        -> Dynamic Content has to be dealt with seperate process management (pm,)
        (e.g. php fpm for php, pm2 for nodejs)
        and reverse proxy back to NGINX process

        -> Low Resource Usage

* Faster Static Resource: static resourses serving without the need to involve any server side languages

* Higher Concurrency

* Interpreting request as URI location (apache - filesystem locations)
    -> Suitable for Load Balancer, Mail Server

## Configuration

* Directive

```
    <directive> <parameters>;
    e.g. server_name mydomain.com;

    3 types of directive:
    Standard
    Array
    Action

```
* Context

```
    <section> {...}
    e.g. http {...}
```