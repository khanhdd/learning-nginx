# Configuration

## Usage

* Virtual Host

```nginx
    server {...}
```

* Location Blocks

```nginx
    # # 1.Exact match
    # location = /greet {
    #   return 200 'Hello from NGINX "/greet" location - EXACT MATCH.';
    # }

    # 2.Preferential Prefix match
    location ^~ /Greet2 {
      return 200 'Hello from NGINX "/greet" location.';
    }

    # # 3.REGEX match - case sensitive
    # location ~* /greet[0-9] {
    #   return 200 'Hello from NGINX "/greet" location - REGEX MATCH.';
    # }

    # 4.PREFIX match - case insensitive
    location /greet {
      return 200 'Hello from NGINX "/greet" location and after slash.';
    }
```

* Variables

```nginx
    Configuration Variables
    set $var 'something';

    NGINX module variables
    $http, $arg, $uri, $date_local
```

* Rewrite/Redirect

```nginx

    rewrite pattern URI <optional-flag>
    e.g. rewrite ^/user/(\w+) /greet/$1 last;

    return statuscode URI
    if statuscode > 300 -> redirect uri
    e.g. return 307 /thumb.png
    [more redirect code](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

```

* Try-file:
  find until path exist -> stop and serve

```nginx
    try_files path1 path2 final;
    (directive in server block and location block)

    e.g. try_files $uri /cat.png /greet /friendy_404;
    (any path not exist -> go to friendly_404)

    Named locations: location @friendly_404 {}
    Call @friendly_404 such in try_files,..
```

*Logging

```nginx
    location uri {
        access_log path_to_file;
        access_log off; #turn-off log
        error_log path_to_file;
    }
```