# Expires
* A response header informing the client or browser how long it can cached that response for.
    * e.g. tell the browser to cache a copy of photo for relatively long time (until it expires) -> improving site load times, reduction in requests to our server

# Compressed Responses with gzip
* Compress a response on a server (gzip) -> reduce its size -> reduces the time it takes for the client to receive that response

## Usage
```nginx
    http {
        gzip on;
        gzip_comp_level 3; \\value should be 3-4
        gzip_types text/css;
        gzip_types text/javascript;

        location ... {
            add_header Vary Accept-Encoding;
        }
    }
```