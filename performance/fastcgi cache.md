# FastCGI Micro Cache

* A simple server side cache that allows us to store dynamic language responses
* Should cache only GET request_method (not POST)
* Single biggest performance enhancement can add to single site 

Usage
```nginx

    #By pass cache with $no_cache
    #Cache key
    $scheme$request_method$host$request_uri
    e.g. https:// GET domain.com /blog/article
```