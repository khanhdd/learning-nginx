# HTTP2 vs HTTP

1. Binary Protocol vs Textual protocol 
   * e.g. (\<h1>Hello\</h1> vs 00111100...)
   * Reducing chance of error during transfer 
   * 
2. Compressed Headers 
   * Reducing transfer time 
  
3. Persistent Connections
   
4. Multiplex Streaming vs Simplex Streaming (Dedicated connection for each resource - HTTP)
   * Combine multiple assets (css, js, html) 
    -> single stream of binary data 
    -> transmitted over a single connection
    
5. Server Push
   * Client/Browser can be informed if assets (css,js) along with the initial request for the page