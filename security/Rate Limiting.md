# Rate Limiting
* Traffic light for incoming connections
* Managing incomming connections to the server for specific reason
    - Add Security - Brute Force Protection
    - Reliability - Prevent Traffic Spikes, DDOS
    - Shaping - Service Priority

# Basic Auth    
* Provide a simple username/password layer to any part of your site.

# Further Hardening NGINX:
https://www.udemy.com/nginx-fundamentals/learn/lecture/3667404#overview